﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RATCore
{
    ///This is the Server tool used as Listener Program 
    ///This would go on the machine you want to Admin remotely
    public class Server : MyCoreBase
    {
        private  TcpListener listener { get; set; }
        private  int connectedClients { get; set; } = 0;


        public Server(IPAddress address, int port) : base(address, port)
        {
            Helpers.MyWriteLine("Server settings address {0} port {1}", AppLoggging.Verbose, address, port );
        }
 

        public override void Run()
        {
            
            Console.Title = $"Server {Environment.MachineName}";

            listener = new TcpListener(address, port);

            Helpers.MyWriteLine("Server starting...");

            listener.Start();
 
            if (listener != null)
            {
                Helpers.MyWriteLine($"Server started. Listening to TCP clients at {address}:{port}");

                // Continue listening.
                while (true)
                {
                    Console.WriteLine($"Waiting for client... {connectedClients} connected at the moment.");
                    ListenForClients() ; // Helper to listen on the background and return.
                }

            }else{
                Helpers.MyWriteLine($"Server start failed at {address}:{port}");
            }
  
        }
 
        private async void ListenForClients() {
            var clientTask = listener.AcceptTcpClientAsync(); // Get the client

            if (clientTask.Result != null)
            {
                connectedClients++;
                Helpers.MyWriteLine($"Client {connectedClients} connected. Waiting for data.");
                var client = clientTask.Result;
                //string message = "";

                await Task.Run( async () => { //Process the client requests in its own thread async 
                    
                    int thisClientID = connectedClients; //BIG BUG: If multiple sesions are open and closed and reopened then the ID can be reused

                    using (NetworkStream stream = client.GetStream())
                    {
                       
                        bool doLoop = true;

                        while (doLoop)
                        {

                            var message = await RecieveAsync(stream);  //wait for communication
                            Helpers.MyWriteLine($"ClientID:{thisClientID} - {message}" , AppLoggging.Verbose);
 
                            string output = "";
                            var command = message.Split(':');
                            switch (command[0].ToLower().Trim())
                            {
                                case "shell":  //Open a shell and run the command 
                                    if(command.Length==2){
                                    
                                        try{
                                            if(command[1].ToString()=="")
                                            {
                                                throw new ArgumentException("Shell requires a program to run as the argument") ;
                                            }

                                            //Start a shell to run the program and send back the response.
                                            IShell shell;
                                            if(Helpers.TargetPlatform==TargetPlatformName.Win)
                                            {
                                                shell = new WinShell();
                                            }else{
                                                shell = new BashShell();
                                            }
                                            output = shell.RunCommand(command[1].ToString()) ;
                                        }catch(Exception ex){
                                            output = string.Format("==={0}{1}{0}{2}", Environment.NewLine, ex.Message, ClientHelp() ) ;
                                        }
                                        
                                    }else{
                                        Helpers.MyWriteLine("Invalide Command send help");
                                        output = string.Format("==={0}{1}{0}{2}", Environment.NewLine, "Invalide Command send help", ClientHelp() ) ;
                                    }

                                    await SendAsync(stream, output ) ; //send response to client

                                    break;
                                case "quit":

                                    output = "Server closing connection...";
                                    await SendAsync(stream, output ) ; //send response to client

                                    doLoop = false; //Break loop

                                    break;
                                case "chat": //display message from client

                                    var chat = "";
                                    int i = 0;
                                    foreach(var s in command){
                                        if(i>0){ //skip the first command
                                            chat += s ;
                                        }
                                        i++;
                                    }

                                    output=$"Chat Message recieved from ClientID:{thisClientID} - {chat}";     
                                    Helpers.MyWriteLine(output);
                                    
                                    await SendAsync(stream, output ) ; //send response to client

                                    break;
                                case "getfile": 
                                    //send a file to the client this should not be to hard to get working
                                    output = $"ClientID:{thisClientID} - Command not yet implemented please see help [enter 'help' for commands]";
                                    await SendAsync(stream, output ) ; //send response to client
                                    break;
                                case "getcam": 
                                    //looking at https://github.com/shimat/opencvsharp as a solution

                                    output = $"ClientID:{thisClientID} - Command not yet implemented please see help [enter 'help' for commands]";
                                    await SendAsync(stream, output ) ; //send response to client
                                    break;
                                case "getmic": 
                                    output = $"ClientID:{thisClientID} - Command not yet implemented please see help [enter 'help' for commands]";
                                    await SendAsync(stream, output ) ; //send response to client
                                    break;
                                case "help":  

                                    output = ClientHelp();  
                                    Helpers.MyWriteLine($"ClientID:{thisClientID} - {output}" , AppLoggging.Verbose);

                                    await SendAsync(stream, output ) ; //send response to client

                                    break;
                                default:
                                    //Blank CRLF sent through send out the header
                                    if(command[0] == Environment.NewLine) {
                                        output = $"ClientID:{thisClientID} - Crossplatform Remote Admin Tool Header [enter 'quit' to terminate] [enter 'help' for commands]";
                                        await SendAsync(stream, output ) ; //send response to client
                                    }else if(command[0] == "") {
                                        //This usualy happens when the client connection is brocken so exit the loop
                                        doLoop = false; //Break loop
                                    }else{
                                        //This is garbage.
                                        output = $"ClientID:{thisClientID} - Unknowen Command please see help [enter 'help' for commands]";
                                        await SendAsync(stream, output ) ; //send response to client
                                    }
 
                                    break;
                            }
   
                        }
                    }

                    Helpers.MyWriteLine($"ClientID:{thisClientID} - Closing connection.");    
                });

                connectedClients--;
                
                //client.GetStream().Dispose();
                Helpers.MyWriteLine($"Closed connection. {connectedClients} clients connected at the moment.");
 
            }
        }

        private string ClientHelp(){
            StringBuilder sb = new StringBuilder() ;
            sb.AppendLine("====HELP==========");
            sb.AppendLine("'quit' to exit"); 
            sb.AppendLine("'help' to display this list"); 
            sb.AppendLine("'shell:[command]' to run a command in the shell remotly eg [shell:dir] [shell:netstat] [shell:ls] "); 
            sb.AppendLine("'chat:[message]' to display a message on screen eg [chat:hello world!]"); 
            sb.AppendLine("====END==========="); 

            return sb.ToString();
        }





#region OPTION4 



        // bool ExitApp = false;

        // public override void Run()
        // {

        //     Console.Title = "Server";

        //     CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        //     CancellationToken cancel = cancellationTokenSource.Token;

        //     TcpListener listener = new TcpListener(address, port);

        //     while (!ExitApp)
        //     {

        //         Helpers.MyWriteLine("Service listening at {0}", AppLoggging.Info, listener.LocalEndpoint);
        //         listener.Start();
                
        //         var task = AcceptClientsAsync(listener, cancel);

        //         //Console.ReadKey();
        //         cancellationTokenSource.Cancel();
        //         task.Wait();
        //     }

        //     Console.WriteLine("Admin end");
        //     Console.ReadKey(true);

        // }

        // public async Task AcceptClientsAsync(TcpListener listener, CancellationToken cancel)
        // {

        //     await Task.Yield();

        //     while (!cancel.IsCancellationRequested && !ExitApp)
        //     {
        //         try
        //         {
        //             var timeoutTask = Task.Delay(2000);
        //             var acceptTask = listener.AcceptTcpClientAsync();

        //             await Task.WhenAny(timeoutTask, acceptTask);
        //             if (!acceptTask.IsCompleted)
        //                 continue;

        //             var client = await acceptTask;
        //             HandleClientAsync(client, cancel);
        //         }
        //         catch (Exception ex)
        //         {
        //             Console.WriteLine("Accepting error: " + ex.ToString());
        //         }
        //     }
        // }

        // public async Task HandleClientAsync(TcpClient client, CancellationToken cancel)
        // {

        //     await Task.Yield();
        //     var local = client.Client.LocalEndPoint.ToString();
        //     Console.WriteLine("Connected " + local);
        //     StreamReader sr = null;
        //     StreamWriter sw = null;
        //     try
        //     {
        //         var stream = client.GetStream();
        //         sr = new StreamReader(stream, Encoding.UTF8);
        //         sw = new StreamWriter(stream, Encoding.UTF8);

        //         while (!cancel.IsCancellationRequested && client.Connected && !ExitApp)
        //         {
        //             //sr = new StreamReader(stream, Encoding.UTF8);
        //             //sw = new StreamWriter(stream, Encoding.UTF8);

        //             var msg = await sr.ReadLineAsync(); ;

        //             if (msg == null)
        //                 continue;

        //             var command = Console.ReadLine();

        //             if (command.ToLower() == "exit")
        //             {
        //                 ExitApp = true;
        //                 break;
        //             }

        //             await sw.WriteLineAsync(command);
        //             await sw.FlushAsync();


        //         }

        //     }
        //     catch (Exception ex)
        //     {
        //         Console.WriteLine("Client error: " + ex.ToString());
        //     }
        //     finally
        //     {
        //         if (sr != null)
        //             sr.Dispose();

        //         if (sw != null)
        //             sw.Dispose();
        //     }
        //     Console.WriteLine("Disconnected " + local);
        // }


#endregion 


        #region OPTION3 

        // public void RunOption3(){

        //    //OPTION 3

        //             Console.WriteLine("Run Client");

        //             try
        //             {
        //                 tcpListener = new TcpListener(System.Net.IPAddress.Any, port);
        //                 tcpListener.Start();
        //                 Console.WriteLine("Listening on address {0} port {1} ...", System.Net.IPAddress.Any, port);

        //                 for (;;)
        //                 {
        //                     Console.WriteLine("Waiting for connection...");

        //                     var sfs = tcpListener.AcceptSocketAsync();
        //                     socketForServer = sfs.Result;
        //                     IPEndPoint ipend = (IPEndPoint)socketForServer.RemoteEndPoint;

        //                     Console.WriteLine("Connection from {0} ...", IPAddress.Parse(ipend.Address.ToString()));

        //                     networkStream = new NetworkStream(socketForServer);
        //                     streamReader = new StreamReader(networkStream);
        //                     streamWriter = new StreamWriter(networkStream);
        //                     strInput = new StringBuilder();

        //                     Console.WriteLine("Start socker read");

        //                     while (true)
        //                     {


        //                         var msg = await streamReader.ReadLineAsync();
        //                         if (msg == null)
        //                             continue;

        //                         strInput.AppendLine(msg);
        //                         Console.WriteLine("MessageRecived:{0}", msg);



        //                         Task tRead = Task.Run(() =>
        //                         {

        //                             try
        //                             {



        //                                 string message = "";
        //                                 while (message != null)
        //                                 {
        //                                     message = streamReader.ReadLine();
        //                                     strInput.AppendLine(message);
        //                                     Console.WriteLine("MessageRecived:{0}", message);
        //                                 }
        //                             }
        //                             catch (Exception err)
        //                             {
        //                                 Console.WriteLine(err.ToString());
        //                                 Cleanup();
        //                             }
        //                             finally { }

        //                         });



        //                         Console.WriteLine(strInput.ToString());
        //                         strInput.Remove(0, strInput.Length);


        //                         var s = Console.ReadLine();

        //                         strInput.Append(s);
        //                         streamWriter.WriteLineAsync(strInput.ToString());
        //                         streamWriter.FlushAsync();

        //                         Console.WriteLine("MessageSent:{0}", s);

        //                         strInput.Remove(0, strInput.Length);


        //                         if (s == "exit") { Cleanup(); break; }
        //                         if (s == "terminate") { Cleanup(); break; }
        //                         //if (s == "cls") ;

        //                     }

        //                 }

        //             }
        //             catch (Exception ex)
        //             {
        //                 Console.WriteLine(ex.ToString());
        //             }


        // }

        #endregion



        #region OPTION2 

        // public static void RunOption2()
        // {

        //     Console.Title = "Server";

        //     CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        //     CancellationToken cancel = cancellationTokenSource.Token;

        //     TcpListener listener = new TcpListener(IPAddress.Any, 5000);
        //     listener.Start();
        //     Console.WriteLine("Service listening at " + listener.LocalEndpoint.ToString());

        //     var task = AcceptClientsAsync(listener, cancel);

        //     Console.ReadKey();
        //     cancellationTokenSource.Cancel();
        //     task.Wait();
        //     Console.WriteLine("end");
        //     Console.ReadKey(true);

        // }

        // public static async Task AcceptClientsAsync(TcpListener listener, CancellationToken cancel)
        // {
        //     //OPTION 2
        //     await Task.Yield();

        //     while (!cancel.IsCancellationRequested)
        //     {
        //         try
        //         {
        //             var timeoutTask = Task.Delay(2000);
        //             var acceptTask = listener.AcceptTcpClientAsync();

        //             await Task.WhenAny(timeoutTask, acceptTask);
        //             if (!acceptTask.IsCompleted)
        //                 continue;

        //             var client = await acceptTask;
        //             HandleClientAsync(client, cancel);
        //         }
        //         catch (Exception aex)
        //         {
        //             var ex = aex.GetBaseException();
        //             Console.WriteLine("Accepting error: " + ex.Message);
        //         }
        //     }
        // }

        // public static async Task HandleClientAsync(TcpClient client, CancellationToken cancel)
        // {
        //     //OPTION 2
        //     await Task.Yield();
        //     var local = client.Client.LocalEndPoint.ToString();
        //     Console.WriteLine("Connected " + local);
        //     StreamReader sr = null;
        //     StreamWriter sw = null;
        //     try
        //     {
        //         var stream = client.GetStream();
        //         sr = new StreamReader(stream, Encoding.UTF8);
        //         sw = new StreamWriter(stream, Encoding.UTF8);
        //         while (!cancel.IsCancellationRequested && client.Connected)
        //         {
        //             //sr = new StreamReader(stream, Encoding.UTF8);
        //             //sw = new StreamWriter(stream, Encoding.UTF8);

        //             var msg = await sr.ReadLineAsync(); ;

        //             if (msg == null)
        //                 continue;

        //             await sw.WriteLineAsync(msg);
        //             await sw.FlushAsync();


        //         }
        //     }
        //     catch (Exception aex)
        //     {
        //         var ex = aex.GetBaseException();
        //         Console.WriteLine("Client error: " + ex.Message);
        //     }
        //     finally
        //     {
        //         if (sr != null)
        //             sr.Dispose();

        //         if (sw != null)
        //             sw.Dispose();
        //     }
        //     Console.WriteLine("Disconnected " + local);
        // }

        #endregion


        #region OPTION1 

        // public static async Task StartClientAsync()
        //         {
        //             //OPTION 1
        //             TcpListener listener = new TcpListener(IPAddress.Any, 1234);
        //             listener.Server.NoDelay = true;
        //             listener.Server.LingerState = new LingerOption(true, 0);
        //             listener.Start();

        //             while (true)
        //             {
        //                 Console.WriteLine("Waiting for client.");

        //                 using (TcpClient client = await listener.AcceptTcpClientAsync())
        //                 {
        //                     client.NoDelay = true;
        //                     client.LingerState = new LingerOption(true, 0);

        //                     Console.WriteLine("Reading message.");
        //                     using (NetworkStream stream = client.GetStream())
        //                     {
        //                         byte[] buffer = new byte[32];
        //                         int len = await stream.ReadAsync(buffer, 0, buffer.Length);
        //                         string incomingMessage = Encoding.UTF8.GetString(buffer, 0, len);
        //                         Console.WriteLine("Incoming message: {0}", incomingMessage);


        //                         Console.WriteLine("Sending message.");
        //                         byte[] message = Encoding.UTF8.GetBytes("Thank you!");
        //                         await stream.WriteAsync(message, 0, message.Length);
        //                         Console.WriteLine("Closing connection.");
        //                     }
        //                 }


        //             }
        //         }

        #endregion



        // private void Cleanup()
        // {
        //     try
        //     {
        //         streamReader.Dispose();
        //         streamWriter.Dispose();
        //         networkStream.Dispose();
        //         socketForServer.Dispose();
        //     }
        //     catch (Exception err)
        //     {
        //         Console.WriteLine(err.ToString());
        //     }

        //     Console.WriteLine("Connection Lost ...");
        // }


        // private void StopClient()
        // {
        //     Cleanup();
        //     System.Environment.Exit(0);
        // }

    }


}
