﻿using System;
using System.Net;


namespace RATCore
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

#if DEBUG
                //easy change startup test parameters for Debug
               //args = new string[4] { "--m:c", "--v", "--p:21597", ""  };
#endif

                foreach (var arg in args)
                {
                    var a = arg.Split(':');

                    switch (a[0])
                    {
                        case "--m":  //Application Mode
                            var mode = string.Format("{0}", a[1]).ToLower();
                            if (mode == "s" || mode == "server")
                            {
                                AppConfig.ApplicationMode = AppMode.Server;
                            }
                            else if (mode == "c" || mode == "client")
                            {
                                AppConfig.ApplicationMode = AppMode.Client;
                            }
                            else
                            {
                                throw new ArgumentException("Application Mode invalide. Please see help.");
                            }
                            break;
                        case "--p":  //Connect or Listen Port
                            if (a.Length == 2) //If the port is not provided the default will be used
                            {
                                bool success = int.TryParse(a[1], out AppConfig.port);
                                if (!success)
                                {
                                    throw new ArgumentException("Port Number invalide. Please see help.");
                                }

                                if (AppConfig.port <= 0 || AppConfig.port > 65535)
                                {
                                    throw new ArgumentException("Port Number must be between 1 and 65535. Please see help.");
                                }
                            }

                            break;
                        case "--a":  //IP to connect to

                            if (a.Length == 2) //If the IP is not provided the default will be used
                            {
                                bool success = IPAddress.TryParse(a[1], out AppConfig.ip);
                                if (!success)
                                {
                                    throw new ArgumentException("IP invalide. Please see help.");
                                }
                            }

                            break;
                        case "--v":
                            AppConfig.Verbose = true;
                            Helpers.MyWriteLine("Verbose mode enabled", AppLoggging.Verbose);
                            break;
                        case "--h":
                        default:
                            displayHelp();
                            break;
                    }
                }


                switch (AppConfig.ApplicationMode)
                {
                    case AppMode.Server:
                        Helpers.MyWriteLine("Execute as Server");
                        var adminTCP = new Server(AppConfig.ip, AppConfig.port);
                        adminTCP.Run();
                        break;
                    case AppMode.Client:
                        Helpers.MyWriteLine("Execute as Client");
                        var clientTCP = new Client(AppConfig.ip, AppConfig.port);
                        clientTCP.Run();
                        break;
                    default:
                        //this should never happen really but just incase lets display help
                        displayHelp();
                        break;
                }

            }
            catch (Exception err)
            {
                Console.WriteLine(err.ToString());
                displayHelp();
            }
            finally
            {
                
                Console.Write("Press <Enter> to exit... ");
                while (Console.ReadKey().Key != ConsoleKey.Enter) {}
                Console.WriteLine("Exit - All done!");
            }

        }


        private static void displayHelp()
        {
            Console.WriteLine("==================================");
            Console.WriteLine(" -- Help -- ");
            Console.WriteLine("-----------------------------");

            Console.WriteLine("--h          Display Help");
            Console.WriteLine("--a          IP Address (default loopback)");
            Console.WriteLine("--p          port (default 21597)");
            Console.WriteLine("--m          Start up Mode (s - Server , c - Client )");

            Console.WriteLine("-----------------------------");

            Console.WriteLine("Client Example:          dotnet run RATCore.dll --m:c --a:192.168.1.10 --p:21597");
            Console.WriteLine("Server Example:          dotnet run RATCore.dll --m:s --p:21597");

            Console.WriteLine("==================================");
        }
    }
}
