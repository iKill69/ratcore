using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RATCore
{
   
    public abstract class MyCoreBase
    {

        public IPAddress address { get; }
        public int port { get; }

        public MyCoreBase(IPAddress address, int port)
        {
            this.address = address;
            this.port = port;
        }

        abstract public void Run();

        protected async Task SendAsync(NetworkStream stream, byte[] bufferWrite )
        {
                Helpers.MyWriteLine($"Message Send: {bufferWrite.Length}",AppLoggging.Verbose);

                await stream.WriteAsync(bufferWrite, 0, bufferWrite.Length);
                await stream.FlushAsync(); //Not sure this actualy helps with network streams but better safe than sorry since it does not hurt

                Helpers.MyWriteLine($"Message Sent: {bufferWrite.Length}",AppLoggging.Verbose);
        }


        protected async Task SendAsync(NetworkStream stream, string message )
        {
                Helpers.MyWriteLine($"Message Send: {message}",AppLoggging.Verbose);

                byte[] bufferWrite = Encoding.ASCII.GetBytes(message);
                await stream.WriteAsync(bufferWrite, 0, bufferWrite.Length);
                await stream.FlushAsync(); //Not sure this actualy helps with network streams but better safe than sorry since it does not hurt

                Helpers.MyWriteLine($"Message Sent: {message}",AppLoggging.Verbose);
        }

        protected async Task<string> RecieveAsync(NetworkStream stream)
        {
             Helpers.MyWriteLine("Waiting for message...",AppLoggging.Verbose);
            
            StringBuilder sb = new StringBuilder();

            //Loog over the buffer for long streams 
            byte[] bufferRead = new byte[1024];
            string messageResponse = "";
            while(true)
            {
                //Do not call this unless there is data expected to arrive at the buffer or it will wait and seem to be frozen
                //Might need to consider custom protocol with a time out rather.
                var lenRead = await stream.ReadAsync(bufferRead, 0, bufferRead.Length);
                messageResponse = Encoding.ASCII.GetString(bufferRead, 0, lenRead);

                sb.Append(messageResponse);

                //Simple fix so I don't need my own protocol
                //lenRead holds the actual length read from the buffer 
                //so we assume if the length is shorter than expeced then it is the last of the stream on the buffer.
                if (lenRead < bufferRead.Length){
                    break;
                } 
            }  

            Helpers.MyWriteLine("Message Recieved. ",AppLoggging.Verbose);

            return sb.ToString();
        }
    }

 

}
