﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RATCore
{
    public enum AppLoggging { Info = 0, Verbose = 1 }
    public enum AppMode { Server = 0, Client = 1 }

    public enum TargetPlatformName { Win = 0, Unix = 1, MacOS = 2, XBox=4, Unknown=5 }

    public static class Helpers
    {

        /// <summary>
        /// I will use this when I need different code paths based on target OS type
        /// </summary>
        public static TargetPlatformName TargetPlatform 
        {
            get
            {
                switch (Environment.OSVersion.Platform)
                {
                    case PlatformID.Win32S:
                    case PlatformID.Win32Windows:
                    case PlatformID.Win32NT:
                    case PlatformID.WinCE:
                        return TargetPlatformName.Win;
                    case PlatformID.Unix:
                    case PlatformID.MacOSX:
                        return TargetPlatformName.Unix;
                    case PlatformID.Xbox:
                        return TargetPlatformName.XBox;
                    default:
                        return TargetPlatformName.Unknown;
                }
            }
        }

        public static void MyWriteLine(string format, AppLoggging LogLevel = AppLoggging.Info, params object[] arg)
        { 
            if(LogLevel == AppLoggging.Verbose){
                if (AppConfig.Verbose)
                {
                    //Only log Verbose msg if app is in the corret mode.
                    Console.WriteLine($"VERBOSE: {format}", arg);
                }
            }else if(LogLevel == AppLoggging.Info){
                Console.WriteLine(format, arg);
            }

        }
        public static void MyWriteLine(string format, AppLoggging LogLevel = AppLoggging.Info)
        {
            
            if(LogLevel == AppLoggging.Verbose){
                if (AppConfig.Verbose)
                {
                    //Only log Verbose msg if app is in the corret mode.
                    Console.WriteLine($"VERBOSE: {format}" );
                }
            }else if(LogLevel == AppLoggging.Info){
                Console.WriteLine(format);
            }

        }

        public static TcpState GetState(this TcpClient tcpClient)
        {
            var con = IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections().SingleOrDefault(x => x.LocalEndPoint.Equals(tcpClient.Client.LocalEndPoint));
            return con != null ? con.State : TcpState.Unknown;
        }
    }

 
    public static class AppConfig
    {
        public static AppMode ApplicationMode = AppMode.Server;
        public static IPAddress ip = IPAddress.Loopback;
        public static int port = 21597;
        public static bool Verbose = false;
    }
 
#region Maybe Implement this later for better controll over the buffer

    public class MyBufferRead: MySimpleProtocol {
        
        public MyBufferRead():base(){
            MessageComplete=false;
        }
 
        public void ReadLine(string message){
            
            MessageComplete=false;
            
            //Pars the first 30 chars to get the actual length of the message coming through 
            if(ProtocolMessageLength==-1){
                //we should only do this the first time
                if(message.Length<30){throw new ArgumentOutOfRangeException("The custom protocol requires the first 30 chars to be reserved so all messages should be at least 30 char long");}

                var lenString = message.Substring(0,30);
                int len=-1;
                int.TryParse(lenString,out len); //this will set the exact length of the arriving message 
                ProtocolMessageLength = len;
            }
  
            //This is the full message
            ProtocolMessage += message ;
            
            Message =  ProtocolMessage.Substring(30);
            MessageLength = Message.Length; //New length

            //Migth as well populat the byte array it could be usefull
            BufferMessage  = Encoding.ASCII.GetBytes(ProtocolMessage);
            BufferMessageLength = BufferMessage.Length;


            if(ProtocolMessageLength==ProtocolMessage.Length){
                //We have the full message
                MessageComplete=true;
            }else{
                //still waiting for more
                MessageComplete=false;
            }
            
         }
    }

    public class MyBufferWrite: MySimpleProtocol {
        public MyBufferWrite():base( ){

        }
        
        public void WriteLine(string message){
            Message = message;
            MessageLength = Message.Length;

            byte[] buffer = Encoding.ASCII.GetBytes(message);

            string reservedBytes = string.Format("{0}", buffer.Length).PadLeft(30,'0');
            reservedBytes = reservedBytes.Substring(reservedBytes.Length-30) ;
 
            ProtocolMessage = reservedBytes + message ;
            ProtocolMessageLength = ProtocolMessage.Length;

            BufferMessage  = Encoding.ASCII.GetBytes(ProtocolMessage);
            BufferMessageLength = BufferMessage.Length;

            MessageComplete=true;
         }
    }

    public class MySimpleProtocol{
        public MySimpleProtocol( ){
          
        }
 
        public byte[] BufferMessage{get; protected set;}
        public int BufferMessageLength{get; protected set;} = -1;
        
        public string ProtocolMessage{get; protected set;}
        public int ProtocolMessageLength{get; protected set;}= -1;

        public string Message{get; protected set;}
        public int MessageLength{get; protected set;}= -1;

        public bool MessageComplete{get; protected set;}=false ;
    }

#endregion    

}
