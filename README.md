# README #

This Application is a dotnet Core Cross Platform Remote Admin Shell proof of concept. I built it as a learning tool. 
I have not had much time to play with this so it is fairly light on commands.
This application functions as both the Client(Master) and Server(Slave) mode.

### What is this repository for? ###

* dotnet core 2.1 rc

### How do I get set up? ###

* Install Git from https://git-scm.com/book/en/v2/Getting-Started-Installing-Git 
* Configuration Git for first use https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup 
* Install dotnet Core https://www.microsoft.com/net/download/dotnet-core/sdk-2.1.300-preview1 
* Install VSCode https://code.visualstudio.com/download 
* Download this source or clone to a folder. [git clone https://bitbucket.org/iKill69/ratcore.git]
* Open VSCode >> File >>> Open Folder... and brows to the new folder
* VSCode Terminal run [dotnet restore]
* [Ctrl + Shift + b] then [Enter] to build
* [F5] to run 

### Crossplatform build ###

VSCode Terminal or in the CLI application directory run the following 

* dotnet restore
* dotnet publish -c Release -r ubuntu.18.04-x64  
* dotnet publish -c Release -r osx.10.11-x64
* dotnet publish -c Release -r win10-x64  
* dotnet publish -c Release -r win-x86
* dotnet publish -c Release -r win10-arm
* dotnet publish -c Release -r linux-x64
* dotnet publish -c Release -r android          <-- runntime package error -->


### Help ###

* --h          Display Help
* --a          IP Address (default loopback)
* --p          port (default 21597)
* --m          Start up Mode (s - Server , c - Client )

* Client Example:          dotnet run RATCore.dll --m:c --a:192.168.1.10 --p:21597
* Server Example:          dotnet run RATCore.dll --m:s --p:21597


### COMMANDS ###

* 'quit' to exit
* 'help' to display this list
* 'shell:[command]' to run a command in the shell remotly eg [shell:dir] [shell:netstat] [shell:ls] 
* 'chat:[message]' to display a message on screen eg [chat:hello world!]



